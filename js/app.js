// Draggable & Droppable
$(document).ready(function(){

	$( "#box" ).draggable({
		revert : "invalid"
	});
	$( "#boxTwo" ).draggable();

		$( "#boxOne" ).droppable({
			accept : "#boxTwo",
			drop : function(){
				alert("Done");
			}

	});

});



//autocomplete
$(document).ready(function(){

	var cityName = ["Dhaka","Comilla","Khulna","Magura","Noakhali","Chittagong"];
	
	$("#city").autocomplete({
		source:cityName
	});

});


// Sortable
$(document).ready(function(){
	$( "#parent" ).sortable();
});


